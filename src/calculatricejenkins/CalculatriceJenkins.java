/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package calculatricejenkins;

/**
 *
 * @author steve
 */
public class CalculatriceJenkins {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
    }
    
    public double add(double a, double b) {
            return a+b;
        }
    
    public double substract(double a, double b) {
            return a-b;
        }
    
    public double multiply(double a, double b) {
            return a*b;
        }
    
    public double divide(double a, double b) {
            return a/b;
        }
    
    
    
}
