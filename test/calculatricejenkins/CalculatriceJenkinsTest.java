/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package calculatricejenkins;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author steve
 */
public class CalculatriceJenkinsTest {
    
    public CalculatriceJenkinsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class CalculatriceJenkins.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        double a = 10.0;
        double b = 20.0;
        CalculatriceJenkins instance = new CalculatriceJenkins();
        double expResult = 30.0;
        double result = instance.add(a, b);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of substract method, of class CalculatriceJenkins.
     */
    @Test
    public void testSubstract() {
        System.out.println("substract");
        double a = 5.0;
        double b = 6.0;
        CalculatriceJenkins instance = new CalculatriceJenkins();
        double expResult = -1.0;
        double result = instance.substract(a, b);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of multiply method, of class CalculatriceJenkins.
     */
    @Test
    public void testMultiply() {
        System.out.println("multiply");
        double a = 20;
        double b = 3.0;
        CalculatriceJenkins instance = new CalculatriceJenkins();
        double expResult = 60;
        double result = instance.multiply(a, b);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of divide method, of class CalculatriceJenkins.
     */
    @Test
    public void testDivide() {
        System.out.println("divide");
        double a = -4.5;
        double b = 2.0;
        CalculatriceJenkins instance = new CalculatriceJenkins();
        double expResult = -2.25;
        double result = instance.divide(a, b);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }
    
}
